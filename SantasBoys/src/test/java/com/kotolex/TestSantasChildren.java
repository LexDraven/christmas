package com.kotolex;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSantasChildren {
    private final SantasChildren children = new SantasChildren();

    @Test
    public void shouldBeOneAsinTask(){
        Assert.assertEquals(1,children.getBadBoysCount("O==ODO=O="));
    }

    @Test
    public void shouldBeTwoBadBoys(){
        Assert.assertEquals(2,children.getBadBoysCount("O==OD=O"));
    }

    @Test
    public void shouldBeNoBadBoys(){
        Assert.assertEquals(0,children.getBadBoysCount("O=DO="));
    }

    @Test
    public void shouldBeZeroForOnlySanta(){
        Assert.assertEquals(0,children.getBadBoysCount("D"));
    }
}
