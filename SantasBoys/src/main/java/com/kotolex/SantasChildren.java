package com.kotolex;

public final class SantasChildren {

    public int getBadBoysCount(String text) {
        int count = 0;
        for (int i = 0; i < text.length(); i++) {
            char atIndex = text.charAt(i);
            if (atIndex == '=') {
                i = i + 1;
                count++;
            }
            if (atIndex == 'O') {
                i = i + 1;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        SantasChildren children = new SantasChildren();
        System.out.println(children.getBadBoysCount("O==ODO=O="));
    }
}
